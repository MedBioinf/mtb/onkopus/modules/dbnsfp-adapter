FROM python:3.12.0b4-bookworm

RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y python3-pip python3-dev tabix less

RUN python3 -m pip install --upgrade pip
RUN python3 -m venv /opt/venv
RUN /opt/venv/bin/python3 -m pip install --upgrade pip

WORKDIR /data
RUN chmod 777 /data

WORKDIR /data
COPY ./preprocess.sh /data/preprocess.sh

WORKDIR /app/dbnsfp-adapter
COPY ./requirements.txt /app/dbnsfp-adapter/requirements.txt
RUN /opt/venv/bin/pip install -r requirements.txt

COPY . /app/dbnsfp-adapter/
CMD ["export", "PYTHONPATH=/app"]

EXPOSE 10122

ENTRYPOINT ["/data/preprocess.sh"]
