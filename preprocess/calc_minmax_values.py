import numpy as np
import gzip, os, sys, json


def get_index(score,header_line):
    elements = header_line.split("\t")
    for i, el in enumerate(elements):
        if el == score:
            print("Index of ",score,": ",i)
            return i

def write_scores_in_file(scores, data_path):
    """
    Retrieves the minimum and maximal value of a list of score values

    :param label:
    :param scores:
    :return:
    """

    for score in scores:
        score_values = []
        index = None
        outfile_src = data_path + "dbNSFP4.4a_variant_" + str(score) + ".csv"
        # get all score values
        for i in range(1, 23):
            print(i)
            filepath = data_path + "dbNSFP4.4a_variant.chr" + str(i) + ".gz"
            #if score not in score_values:
            #    score_values[score] = []

            with gzip.open(filepath,'rt') as f:
                #with open(outfile,"w") as out:
                    for line in f:
                        if line.startswith("#"):
                            header_line = line
                        if index is None:
                            index = get_index(score, header_line)
                        elements = line.split("\t")
                        score_val = elements[index]
                        if score_val != ".":
                            vals = score_val.split(";")
                            for v in vals:
                                if v != ".":
                                    #print(v,file=out)
                                    try:
                                        float_val = float(v)
                                        score_values.append(float_val)
                                    except:
                                        print("could not convert to float: ",str(v))
        #print("scores for ",str(i) + ": ", score_values[score])
        outfile = open(outfile_src,"w")
        json.dump(score_values, outfile)
        outfile.close()
    index = None


def calc_score_stats(scores, data_path):
    minmax_file = data_path + "scores_stats.csv"
    scores_minmax = {}

    for score in scores:
        infile = open(data_path + "dbNSFP4.4a_variant_" + str(score) + "_scaled_comb.csv","r")
        score_values = json.load(infile)

         # get min and max values
        score_min = np.min(score_values["values_raw"])
        score_max = np.max(score_values["values_raw"])
        std = np.std(score_values["values_raw"])
        m = np.mean(score_values["values_raw"])
        score_min_sc = np.min(score_values["values_scaled"])
        score_max_sc = np.max(score_values["values_scaled"])
        std_sc = np.std(score_values["values_scaled"])
        m_sc = np.mean(score_values["values_scaled"])
        print(score_min, ":", score_max, ":", std, ":", m)
        if score not in scores_minmax:
            scores_minmax[score] = {"min": score_min, "max":score_max, "std": std, "mean":m, "min_scaled": score_min_sc, "max_scaled":score_max_sc, "std_scaled": std_sc, "mean_scaled":m_sc }
        else:
            if score_min < scores_minmax[score]["min"]:
                scores_minmax[score]["min"] = score_min
            if score_max > scores_minmax[score]["max"]:
                scores_minmax[score]["max"] = score_max
            if score_min_sc < scores_minmax[score]["min_scaled"]:
                scores_minmax[score]["min_scaled"] = score_min_sc
            if score_max_sc > scores_minmax[score]["max_scaled"]:
                scores_minmax[score]["max_scaled"] = score_max_sc

    print(scores_minmax)
    with open(minmax_file, "w") as out:
        row = "score,min,max,std,mean,min_scaled,max_scaled,std_scaled,mean_scaled"
        print(row, file=out)
        for score in scores_minmax.keys():
            row = str(score) + "," + str(scores_minmax[score]["min"]) + "," + str(scores_minmax[score]["max"]) \
                + "," + str(scores_minmax[score]["std"]) + "," + str(scores_minmax[score]["mean"]) \
                + "," + str(scores_minmax[score]["min_scaled"]) + "," + str(scores_minmax[score]["max_scaled"]) \
                + "," + str(scores_minmax[score]["std_scaled"]) + "," + str(scores_minmax[score]["mean_scaled"])
            print(row, file=out)

def scale_values(scores, data_path):
    """
    Scales selected scores to a range of [0,1]

    :param variant_data:
    :return:
    """
    minmax_file = data_path + "scores_stats.csv"
    scores_minmax = {}

    for score in scores:
        infile = open(data_path + "dbNSFP4.4a_variant_" + str(score) + ".csv","r")
        outfile_src = data_path + "dbNSFP4.4a_variant_" + str(score) + "_scaled_comb.csv"
        score_values = json.load(infile)
        score_values_scaled = []

        score_min = np.min(score_values)
        score_max = np.max(score_values)
        std = np.std(score_values)
        m = np.mean(score_values)

        for val in score_values:
            score_scaled = (val - score_min) / (score_max - score_min)
            try:
                float_val = float(score_scaled)
                score_values_scaled.append(float_val)
            except:
                print("could not convert to float: ", str(score_scaled))

        score_values_comb = { "values_raw": score_values, "values_scaled": score_values_scaled }

        outfile = open(outfile_src, "w")
        json.dump(score_values_comb, outfile)
        outfile.close()



scores_labels = []
scores_indices = []

if __name__ == "__main__":
    scores = ["REVEL_score",
              "PrimateAI_score",
              "Polyphen2_HDIV_score",
              "Polyphen2_HVAR_score",
              "MutationTaster_score",
              "MutationAssessor_score",
              "MutPred_score",
              "SIFT_score",
              "VARITY_ER_score",
              "VARITY_R_score",
              "VARITY_R_LOO_score",
              "VARITY_ER_LOO_score",
              "bStatistic",
              "phyloP17way_primate",
              "CADD_raw",
              "CADD_phred",
              "BayesDel_addAF_score",
              "ClinPred_score",
              "DANN_score",
              "DEOGEN2_score",
              "Eigen-PC-phred_coding",
              "GERP++_RS",
              "FATHMM_score",
              "M-CAP_score",
              "MPC_score",
              "MetaLR_score",
              "PROVEAN_score",
              "MetaSVM_score",
              "LRT_score",
              "phastCons17way_primate"
              ]
    data_path = os.environ["DATA_PATH"]
    write_scores_in_file(scores, data_path)
    scale_values(scores, data_path)
    calc_score_stats(scores, data_path)
