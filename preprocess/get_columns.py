import gzip
import os


def get_columns(filepath):
    """
    Extracts column identifiers from header line of database file

    :param filepath:
    :return:
    """
    with gzip.open(filepath,'rt') as f:
        line = f.readline()
        columns = line.split("\t")
    return columns

infile=os.getenv("data_path") + "/dbNSFP4.4a_variant.ch1.gz"
outfile=os.getenv("data_path") + "/columns.txt"
print(",".join(get_columns(infile)),file=open(outfile,"w"))
