import sys, traceback, os, gzip


def generate_columns(input_file, output_file):
    print("Update dbNSFP columns, input file: ",input_file," output file ",output_file)
    try:
        with gzip.open(infile, 'rt') as file:
            first_line = file.readline().strip()
            data_list = first_line.split('\t')
            with open(output_file, 'w') as output:
                output.write(','.join(data_list))
            print("Columns have been updated")
    except FileNotFoundError:
        print(traceback.format_exc())

if __name__ == "__main__":
    infile = sys.argv[1]
    data_path = sys.argv[2]
    version = sys.argv[3]
    __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
    outfile = data_path + "/columns_" + version + ".txt"
    generate_columns(infile, outfile)
