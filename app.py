from flask import request
from flask_cors import CORS
from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
import dbnsfp_adapter.data_parser as parser

DEBUG = True
SERVICE_NAME="dbnsfp-adapter"
VERSION="v1"

app = Flask(__name__)
app.config.from_object(__name__)

CORS(app, resources={r'/*': { 'origins': '*' }})
SWAGGER_URL = f'/{SERVICE_NAME}/{VERSION}/docs'
API_URL = '/static/config.json'

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "dbNSFP-Adapter"
    },
)

# definitions
SITE = {
        'logo': 'FLASK-VUE',
        'version': '0.0.1'
}


@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/full', methods=['GET'])
def get_score(genome_version=None):
    variant = request.args.get("genompos")
    variant_list = variant.split(",")
    response = parser.get_dbnsfp_data(variant_list)
    return response


@app.route(f'/{SERVICE_NAME}/{VERSION}/info')
def info_endpoint():
    info_data = {}
    info_data["title"] = "dbNSFP Service"
    info_data["database_version"] = "dbNSFP4.5a"

    return info_data

if __name__ == '__main__':
    app.register_blueprint(swaggerui_blueprint)
    app.run(host='0.0.0.0', debug=True, port=10122)
