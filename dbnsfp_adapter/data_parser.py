import re
import re, subprocess
import traceback

import dbnsfp_adapter.conf.read_config as conf_reader
from dbnsfp_adapter.tools import calculate_score_percentage,format_ensemble_gene_id,format_ensemble_protein_ids,format_ensemble_transcript_ids


def generate_tabix_query(variant_list):
    """


    :param variant_list:
    :return:
    """
    response = {}
    genome_pos_exp = "(chr)([0-9|X]+):([0-9]+)([A|G|C|T]+)>([A|G|C|T]+)"
    p = re.compile(genome_pos_exp)

    tabix_queries = {}
    for variant in variant_list:
        response[variant] = {}

        try:
            groups = p.match(variant)
            chrom = groups.group(2)
            pos = groups.group(3)
            ref = groups.group(4)
            alt = groups.group(5)

            if chrom not in tabix_queries:
                tabix_queries[chrom] = ""

            tabix_queries[chrom] += chrom + ":" + pos + "-" + pos + " "
        except:
            print("Could not parse genomic location: ",variant)
            print(traceback.format_exc())

    return tabix_queries, response


def generate_json_obj_from_database_array(elements):
    """
    Generates a JSON dictionary from a dbNSFP database entry array

    :param elements:
    :return:
    """
    json_obj = {}

    for i,el in enumerate(conf_reader.col_names):
        index = conf_reader.col_names.index(el)
        #if len(list) > index:
        json_obj[el] = elements[index]
        #else:
        #    print("Could not find index: ",index)
        #    json_obj[el] = ""

    return json_obj


def get_dbnsfp_data(variant_list):
    """
    Runs a tabix request

    :param variant_list:
    :return:
    """
    tabix_queries, response = generate_tabix_query(variant_list)

    for chrom in tabix_queries.keys():
        db_file = conf_reader.__DATABASE_PATH__ + "/" + "dbNSFP" + conf_reader.__VERSION__ + "_variant.chr" \
                  + chrom + conf_reader.__DATABASE_FILE_HG38_SUFFIX__

        tabix_query = "tabix " + db_file + " "+ tabix_queries[chrom]

        process = subprocess.Popen(tabix_query.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        output = output.decode("utf-8")

        results = output.split('\n')
        for result in results:
            elements = result.split("\t")
            if len(elements) > 1:
                chrom_res = elements[0]
                pos_res = elements[1]
                ref_res = elements[2]
                alt_res = elements[3]
                var = "chr" + chrom_res + ":" + pos_res + ref_res + ">" + alt_res
                if var in variant_list:
                    response[var][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]] = generate_json_obj_from_database_array(elements)

    response = calculate_score_percentage(response)
    response = format_ensemble_gene_id(response)
    response = format_ensemble_protein_ids(response)
    response = format_ensemble_transcript_ids(response)

    return response
