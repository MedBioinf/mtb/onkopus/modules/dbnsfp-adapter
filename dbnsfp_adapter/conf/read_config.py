import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

if "VERSION" in os.environ:
    __VERSION__ = os.getenv("VERSION")
else:
    __VERSION__ = config['DEFAULT']['VERSION']

if "DATA_PATH" in os.environ:
    __DATABASE_PATH__ = os.getenv("DATA_PATH")
else:
    __DATABASE_PATH__ = config['DEFAULT']['DATABASE_PATH']

if "DATABASE_FILE_URL" in os.environ:
    __DATABASE_FILE_URL__ = os.getenv("DATABASE_FILE_URL")
else:
    __DATABASE_FILE_URL__ = config['DEFAULT']['DATABASE_FILE_URL']

if "DATABASE_FILE_HG38_SUFFIX" in os.environ:
    __DATABASE_FILE_HG38_SUFFIX__ = os.getenv("DATABASE_FILE_HG38_SUFFIX")
else:
    __DATABASE_FILE_HG38_SUFFIX__ = config['DEFAULT']['DATABASE_FILE_HG38_SUFFIX']

def get_columns():
    __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
    col_file = __DATABASE_PATH__ + "/columns_" + __VERSION__ + ".txt"
    with open(col_file, 'r') as f:
        line = f.readline()
        columns = line.split(",")
    return columns
col_names = get_columns()

