import traceback
import dbnsfp_adapter.conf.read_config as conf_reader


def calculate_score_percentage(biomarker_data):

    for qid in biomarker_data.keys():

        if conf_reader.config["DEFAULT"]["MODULE_PREFIX"] not in biomarker_data[qid]:
            biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]] = {}

        # GERP
        if 'GERP++_RS_rankscore' in biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]]:
            try:
                biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]]['gerp_rs_rankscore_percentage'] = \
                    int(float(biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]]['GERP++_RS_rankscore']) * 100)
            except:
                print("Could not parse score ",traceback.format_exc())

        # SIFT
        if 'SIFT4G_converted_rankscore' in biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]]:
            try:
                if biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]][
                                  'SIFT_converted_rankscore'] != '.':
                    biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]]['SIFT_converted_rankscore_percentage'] = \
                        int(float(biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]][
                                      'SIFT_converted_rankscore']) * 100)
                if biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]][
                            'SIFT4G_converted_rankscore'] != '.':
                    biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]]['SIFT4G_converted_rankscore_percentage'] = \
                        int(float(biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]][
                                      'SIFT4G_converted_rankscore']) * 100)
            except:
                print("Could not parse score ",traceback.format_exc())

        # MutationAssessor
        try:
            biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]]['mutationassessor_rankscore_percentage'] = \
                int(float(biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]]['MutationAssessor_rankscore']) * 100)

            # MetaSVM
            biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]]['MetaSVM_rankscore_percentage'] = \
                int(float(biomarker_data[qid][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]][
                              'MetaSVM_rankscore']) * 100)
        except:
            print("error calculating percentage scores")

    return biomarker_data


def format_ensemble_gene_id(json_obj):
    """
    Extracts the first ID of the Ensemble gene ID element

    :param json_obj:
    :return:
    """
    key = "Ensembl_geneid"
    for var in json_obj.keys():
        if key in json_obj[var]["dbnsfp"]:
            if len(json_obj[var]["dbnsfp"][key]) >0:
                json_obj[var]["dbnsfp"][key+"_formatted"] = json_obj[var]["dbnsfp"][key].split(";")[0]
    return json_obj


def format_ensemble_protein_ids(json_obj):
    """
    Extracts the first ID of the Ensemble protein ID element

    :param json_obj:
    :return:
    """
    key = "Ensembl_proteinid"
    for var in json_obj.keys():
        if key in json_obj[var]["dbnsfp"]:
            if len(json_obj[var]["dbnsfp"][key]) >0:
                json_obj[var]["dbnsfp"][key+"_formatted"] = json_obj[var]["dbnsfp"][key].split(";")
    return json_obj


def format_ensemble_transcript_ids(json_obj):
    """
    Extracts the first ID of the Ensemble transcript ID element

    :param json_obj:
    :return:
    """
    key = "Ensembl_transcriptid"
    for var in json_obj.keys():
        if key in json_obj[var]["dbnsfp"]:
            if len(json_obj[var]["dbnsfp"][key]) >0:
                json_obj[var]["dbnsfp"][key+"_formatted"] = json_obj[var]["dbnsfp"][key].split(";")
    return json_obj


def extract_column_labels(infile_src):
    """
    Extracts the dbNSFP column labels

    :param infile_src:
    :return:
    """
    import gzip
    with gzip.open(infile_src, 'rt') as f:
        for line in f:
            columns = line.split("\t")
            print(columns)
            return columns
            break

