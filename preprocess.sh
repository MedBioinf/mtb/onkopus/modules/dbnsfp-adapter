#!/bin/bash

VERSION="${VERSION}"
FILE_NAME="dbNSFP"
FILE_NAME_FULL="${FILE_NAME}${VERSION}.zip"

# Download database file
if [ ! -f "${DATA_PATH}/${FILE_NAME_FULL}" ]
then
  echo "Could not find database file. Starting file download..."
  wget -v "${DATABASE_FILE_URL}" -P ${DATA_PATH} -O "${FILE_NAME_FULL}"
  mv -v "${FILE_NAME_FULL}" "${DATA_PATH}/${FILE_NAME_FULL}"
  FILE_NAME_UNZIP="${FILE_NAME}"
  # Unzip main package
  cd "${DATA_PATH}"
  unzip "${DATA_PATH}/${FILE_NAME_FULL}"
else
  echo "Database file found: ${DATA_PATH}/${FILE_NAME_FULL}"
fi

FILE_NAME_UNZIP="dbNSFP${VERSION}_variant.chr1.gz.tbi"
if [ ! -f "${DATA_PATH}/${FILE_NAME_UNZIP}" ]
then
  # Generate indices for chromosome files
  DBFILES=(
    "chr10"
    "chr11"
    "chr12"
    "chr13"
    "chr14"
    "chr15"
    "chr16"
    "chr17"
    "chr18"
    "chr19"
    "chr1"
    "chr20"
    "chr21"
    "chr22"
    "chr2"
    "chr3"
    "chr4"
    "chr5"
    "chr6"
    "chr7"
    "chr8"
    "chr9"
    "chrX"
    "chrY"
    "chrM"
  )
  for i in "${DBFILES[@]}"; do
    FILE_NAME_UNZIP="dbNSFP${VERSION}_variant.${i}"
    echo "Generate tabix file for ${FILE_NAME_UNZIP}"

    FILE_NAME="${FILE_NAME_UNZIP}.gz"
    FILE="${DATA_PATH}/${FILE_NAME}"
    FILE_UNZIP="${DATA_PATH}/${FILE_NAME_UNZIP}"
    FILE_UNZIP_SORTED="${FILE_UNZIP}.sorted"

    gunzip "${FILE}"
    sort -k1,1 -k2,2n -o "${FILE_UNZIP_SORTED}" "${FILE_UNZIP}"
    mv -v "${FILE_UNZIP_SORTED}" "${FILE_UNZIP}"
    bgzip "${FILE_UNZIP}"
    tabix -s 1 -b 2 -e 2 -p vcf "${FILE_UNZIP}.gz"
  done
  python3 generate_columns.py "${DATA_PATH}/dbNSFP${VERSION}_variant.chr1.gz" "${DATA_PATH}" "${VERSION}"
else
  echo "Database tabix file found: ${DATA_PATH}/${FILE_NAME_UNZIP}"
fi

if [ ! -f "${DATA_PATH}/columns_${VERSION}.txt" ]
then
  echo "Column file not found. Generating file..."
  python3 generate_columns.py "${DATA_PATH}/dbNSFP${VERSION}_variant.chr1.gz" "${DATA_PATH}" "${VERSION}"
fi

/opt/venv/bin/python3 /app/dbnsfp-adapter/app.py
