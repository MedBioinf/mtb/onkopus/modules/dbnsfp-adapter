import unittest
import dbnsfp_adapter.data_parser as parser

class TestTabixQueries(unittest.TestCase):

    def test_tabix_query_generation(self):
        variant_list=["chr14:67885931T>G","chr7:140753336A>T"]
        tabix_queries, response = parser.generate_tabix_query(variant_list)
        print(tabix_queries)
        print(response)

    def test_database_requests(self):
        variant_list = ["chr14:67885931T>G", "chr7:140753336A>T"]
        response = parser.get_dbnsfp_data(variant_list)
        print(response)