import unittest
from app import app

class TestFlaskService(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_flask_service(self):
        genompos="chr14:67885931T>G,chr7:140753336A>T"
        genome_version="hg38"

        response = self.client.get("/dbnsfp-adapter/v1/"+genome_version+"/full?genompos="+genompos)
        print(response.get_data(as_text=True))

