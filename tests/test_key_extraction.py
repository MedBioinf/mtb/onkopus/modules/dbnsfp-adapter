import unittest
import dbnsfp_adapter.data_parser
import dbnsfp_adapter.conf.read_config as conf_reader



class TestKeyExtraction(unittest.TestCase):


    def test_key_extraction(self):
        genompos="chr14:67885931T>G,chr7:140753336A>T"
        genome_version="hg38"
        list = genompos.split(",")

        for key in conf_reader.col_names:
            if key not in conf_reader.all_col_names:
                print("no match ",key)


