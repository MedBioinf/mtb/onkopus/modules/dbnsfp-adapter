# dbNSFP-Adapter

Module for querying the dbNSFP database [1].
The module requires an environment variable DATA_PATH to be set, defining the path on the local file system where the dbNSFP data should be stored.
Optionally, the dbNSFP database version may be defined by setting the VERSION and DATABASE_FILE_URL environment variables, 
which define the version number and a URL where to download the data file. 
If VERSION is not set, the adapter takes the version that is set as the default value (currently: 4.5a).

(Default port: 10122)

### Preprocessing

If the dbNSFP adapter is started for the first time, the adapter will download and extract the data file (~36GB), and 
generates the tabix indices. 
Please be aware that downloading the database and generating the tabix indices may take up to several hours.

### Run locally

Run the dbNSFP adapter locally by defining the configuration environment variables and running the Flask service (app.py): 
```commandline
export DATA_PATH=/local/path/to/dbnsfp/database/
python3 app.py
```

### Build and run Docker container

```commandline
```

### References

Liu, X., Li, C., Mou, C., Dong, Y., & Tu, Y. (2020). dbNSFP v4: a comprehensive database of transcript-specific functional predictions and annotations for human nonsynonymous and splice-site SNVs. Genome medicine, 12(1), 1-8.
